import React from 'react';

const ProfileTable = ({ imageUrl, email, totalScore, biodata, city }) => (
  <div className="table-container mb-8">
    <img src={imageUrl} alt="Profil Pengguna"  />
    <table className="table">
      <tbody>
        <tr>
          <td><strong>Email</strong></td>
          <td>{email}</td>
        </tr>
        <tr className="my-3">
          <td><strong>Total Skor</strong></td>
          <td>{totalScore}</td>
        </tr>
        <tr>
          <td><strong>Biodata</strong></td>
          <td>{biodata}</td>
        </tr>
        <tr>
          <td><strong>Kota</strong></td>
          <td>{city}</td>
        </tr>
      </tbody>
    </table>
  </div>
);

export default ProfileTable;

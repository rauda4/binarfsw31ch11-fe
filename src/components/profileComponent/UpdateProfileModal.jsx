import React from 'react';
import { Modal, Form, Button, Spinner } from 'react-bootstrap';

const UpdateProfileModal = ({
  showModal,
  handleClose,
  handleSubmit,
  isLoading,
  email,
  setEmail,
  totalScore,
  setTotalScore,
  biodata,
  setBiodata,
  city,
  setCity,
  setSelectedImage,  // Remove unused variable
}) => (
    <Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Perbarui Profil</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="formEmail">
            <Form.Label>Email:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukkan email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formTotalScore">
            <Form.Label>Total Skor:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukkan total skor"
              value={totalScore}
              onChange={(e) => setTotalScore(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBiodata">
            <Form.Label>Biodata:</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={biodata}
              onChange={(e) => setBiodata(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formCity">
            <Form.Label>Kota:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukkan kota"
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formImage">
            <Form.Label>Image:</Form.Label>
            <Form.Control
              type="file"
              accept=".png, .jpg, .jpeg"
              onChange={(e) => setSelectedImage(e.target.files[0])}
            />
          </Form.Group>
          {isLoading ? (
            <Button className="mt-3 mb-3" variant="primary" disabled>
              <Spinner animation="border" size="sm" /> Processing...
            </Button>
          ) : (
            <Button className="mt-3 mb-3" variant="primary" type="submit">
              Submit
            </Button>
          )}
        </Form>
      </Modal.Body>
    </Modal>
);


export default UpdateProfileModal;

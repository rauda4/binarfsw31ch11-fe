import React from 'react';
import { Document, Page, View, Text, Image } from '@react-pdf/renderer';

const ProfilePdf = ({ username, email, totalScore, biodata, city, imageUrl }) => (
    <Document>
      <Page size="A4">
        <View>
          <Text>{`Profil Pengguna\n\nUsername: ${username}\nEmail: ${email}\nTotal Skor: ${totalScore}\nBiodata: ${biodata}\nKota: ${city}`}</Text>
          {imageUrl && <Image src={imageUrl} style={{ width: 100, height: 'auto' }} />}
        </View>
      </Page>
    </Document>
);

export default ProfilePdf;

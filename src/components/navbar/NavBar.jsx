// NavBar.jsx
import React from 'react';
import { Nav, Navbar, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import {BiHomeAlt} from 'react-icons/bi'

const NavBar = ({ onLogout }) => {
  const navigate = useNavigate();

  const handleLogout = () => {
    // Perform logout action
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    onLogout(); // Panggil callback onLogout
    navigate('/');
    window.location.reload()
  };

  const isUserLoggedIn = localStorage.getItem('token');
  return (
    <Navbar collapseOnSelect expand="lg" className="navbar" bg="dark" data-bs-theme="dark">
      <Container >
        <Navbar.Brand as={Link} to="/">
          <BiHomeAlt/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/game/rooms">
              Game List
            </Nav.Link>
            {/* <Nav.Link as={Link} to="/gamedetail">
              Game Detail
            </Nav.Link> */}
          </Nav>
          <Nav>
            {isUserLoggedIn ? (
              <>
                <Nav.Link onClick={handleLogout}>Sign Out</Nav.Link>
                <Nav.Link as={Link} to="/profile">Profile </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link as={Link} to="/auth/register">
                  Register
                </Nav.Link>
                <Nav.Link as={Link} to="/auth/login">
                  Login
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;

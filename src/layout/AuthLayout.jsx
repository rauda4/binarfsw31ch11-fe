import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Footer from '../components/navbar/Footer'
import NavBar from '../components/navbar/NavBar'
import Login from '../pages/authentication/Login'
import Register from '../pages/authentication/Register'
import NotFound from '../pages/NotFound'

export default function AuthLayout() {
  return (
    <>
    <NavBar/>
    <Routes>
      <Route path='/login' element={<Login/>}/>
      <Route path='/register' element={<Register/>}/>
      <Route path="*" element={<NotFound/>}/>
    </Routes>
    <Footer/>
    </>
  )
}

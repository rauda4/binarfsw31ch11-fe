import React, { useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import Footer from '../components/navbar/Footer'
import NavBar from '../components/navbar/NavBar'
import Landing from '../pages/home/Landing';
import NotFound from '../pages/NotFound';
import Profile from '../pages/user/Profile';

export default function AuthLayout() {
  const [isLoggedOut, setIsLoggedOut] = useState(false);

  const handleLogout = () => {
    setIsLoggedOut(false);
  };
  return (
    <>
    <NavBar onLogout={handleLogout} />
    <Routes>
      <Route path="/" element={<Landing isLoggedOut={isLoggedOut} />} />
      <Route path="/profile" element={<Profile />} />
      <Route path="/*" element={<NotFound/>}/>
    </Routes>
    <Footer/>
    </>
  )
}

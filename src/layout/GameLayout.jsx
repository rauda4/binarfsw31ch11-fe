import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Footer from '../components/navbar/Footer'
import NavBar from '../components/navbar/NavBar'
import GameDetail from '../pages/game/GameDetail'
import GameList from '../pages/game/GameList'
import NotFound from '../pages/NotFound'

export default function AuthLayout() {
  return (
    <>
    <NavBar />
    <Routes>
        <Route path="/rooms" element={<GameList />} />
        <Route path="/rooms/:id" element={<GameDetail />} />
        <Route path="*" element={<NotFound/>}/>
    </Routes>
    <Footer/>
    </>
  )
}

import React, { useState } from "react";
import { Container, Row, Col, Card, Form, Button, Alert, Spinner }from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState(""); // Ini adalah useState untuk password
  const [email, setEmail] = useState("");
  const [totalScore, setTotalScore] = useState("");
  const [biodata, setBiodata] = useState("");
  const [city, setCity] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [error, setError] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    try {
      const formData = new FormData();
      formData.append("username", username);
      formData.append("password", password);
      formData.append("email", email);
      formData.append("biodata", biodata);
      formData.append("city", city);
      formData.append("image", selectedImage);

      const response = await axios.post("https://31be-production.up.railway.app/auth/regist", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        navigate("/auth/login");
      } else {
        setError(response.data.message);
      }
    } catch (error) {
      if (error.response) {
        setError(error.response.data.msg); // Ubah sesuai dengan struktur pesan error yang dikembalikan oleh server
      } else if (error.message === "Network Error") {
        setError("Terjadi masalah jaringan. Mohon coba lagi.");
      } else {
        setError("Terjadi kesalahan. Mohon coba lagi.");
      }
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
      <Container>
        <Row className="justify-content-md-center mt-5">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title className="text-center mb-4">Register</Card.Title>
                {error && <Alert variant="danger">{error}</Alert>}
                {successMessage && (
                  <Alert variant="success">{successMessage}</Alert>
                )}
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter username"
                      value={username}
                      onChange={(e) => setUsername(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="password" className="mt-4">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Enter password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="email" className="mt-4">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="biodata" className="mt-4">
                    <Form.Label>Biodata</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Enter biodata"
                      value={biodata}
                      onChange={(e) => setBiodata(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="city" className="mt-4">
                    <Form.Label>City</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter city"
                      value={city}
                      onChange={(e) => setCity(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="image" className="mt-4">
                    <Form.Label>Image</Form.Label>
                    <Form.Control
                      type="file"
                      accept=".png, .jpg, .jpeg"
                      onChange={(e) => setSelectedImage(e.target.files[0])}
                    />
                  </Form.Group>

                  {isLoading ? (
                    <Button className="mt-3 mb-3" variant="primary" disabled>
                      <Spinner animation="border" size="sm" /> Processing...
                    </Button>
                  ) : (
                    <Button className="mt-5 mb-3" variant="primary" type="submit">
                      Submit
                    </Button>
                  )}
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
  );
};

export default Register;
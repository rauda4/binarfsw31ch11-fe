import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Alert, Card, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import jwtDecode from "jwt-decode";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false); // Tambahkan state untuk isLoading

  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true); // Set isLoading true ketika proses login dimulai

    try {
      const response = await axios.post("https://31be-production.up.railway.app/auth/login", {
        username: username,
        password: password,
      });

      if (response.status === 200) {
        // Handle successful login
        // console.log(response.data); // You can store the token or perform any other necessary action
        const token = response.data.token;
        localStorage.setItem("token", token);
        const userLogin = jwtDecode(token);
        console.log(userLogin);
        localStorage.setItem("userId", userLogin.id);
        localStorage.setItem("username", userLogin.username);
        // localStorage.setItem("password", userLogin.password);
        // localStorage.setItem("email", userLogin.email);
        // localStorage.setItem("total_score", userLogin.total_score);
        // localStorage.setItem("biodata", userLogin.biodata);
        // localStorage.setItem("city", userLogin.city);
        navigate("/");
      } else {
        setError(response.data.msg);
      }
    } catch (error) {
      setError(error.response.data.msg);
      console.log(error.response.data.msg);
    } finally {
      setIsLoading(false); // Set isLoading false ketika proses login selesai (baik berhasil atau gagal)
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      navigate("/");
    }
  }, [navigate]);

  return (
    <>
      <Container>
        <Row className="justify-content-md-center mt-5">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title className="text-center mb-4">Login</Card.Title>
                {error && <Alert variant="danger">{error}</Alert>}
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter username"
                      value={username}
                      onChange={(e) => setUsername(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="password" className="mt-4">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Enter password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </Form.Group>

                  {/* Tampilkan spinner jika isLoading true */}
                  {isLoading ? (
                    <Button className="mb-3 mt-3" variant="primary" disabled>
                      <Spinner animation="border" size="sm" /> Logging in...
                    </Button>
                  ) : (
                    <Button className="mb-3 mt-3" variant="primary" type="submit">
                      Login
                    </Button>
                  )}
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Login;

import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Nav, Col, Row } from "react-bootstrap";
import CardGame from "../../components/navbar/CardGame";

export default function GameList () {
  const [games, setGames] = useState([]);

  const getGamesData = async () => {
    try {
      const response = await axios.get("https://31be-production.up.railway.app/game/rooms");
      setGames(response.data.rooms);
    } catch (error) {
      console.log(error);
    }
  }
  // const navigate = useNavigate();
  useEffect(() => {
    getGamesData();
    // const token = localStorage.getItem("token");
    // if (!token) {
    //   navigate("/login");
    // }
  },[]);

  // console.log(games);

  return (
    <>
      <Nav className="bg">
        <Row>
          <Col className="left">
            {/* <Button className="btn-ply" href="/games">
            View More Game
          </Button>      */}
          </Col>
          <Col className="right"> </Col>
        </Row>
      </Nav>
      <main className="pt-5 w-100 m-0 list-game">
        <Row className="px-3 gap-3 m-0 justify-content-center">
          {games.map((game) => (
            <Col
              key={game.id}
              xs={12}
              sm={6}
              md={4}
              lg={3}
              className=" d-flex justify-content-center p-0 m-0"
            >
              <CardGame
                title={game.name}
                image={game.thumbnail_url}
                description={game.description}
                url={game.id}
              />
            </Col>
          ))}
        </Row>
      </main>
    </>
  );
};


import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Button, Col, Container, Row } from "react-bootstrap";
import Layout from "../../../layout/Layout";

const RpsGame = () => {
  const [resultGame, setResultGame] = useState("VS");

  const handlePlayerPick = (choice) => {
    reset();
    const result = checkResult(choice);
    showResult(result);
  };

  const checkResult = (playerChoice) => {
    const compRandom = Math.floor(Math.random() * 3);
    const compChoice = computerChoices[compRandom];

    disableComp(compChoice);

    if (compChoice === playerChoice) {
      return "draw";
    } else if (
      (compChoice === "kertas" && playerChoice === "batu") ||
      (compChoice === "batu" && playerChoice === "gunting") ||
      (compChoice === "gunting" && playerChoice === "kertas")
    ) {
      return "comp win";
    } else if (
      (compChoice === "kertas" && playerChoice === "gunting") ||
      (compChoice === "batu" && playerChoice === "kertas") ||
      (compChoice === "gunting" && playerChoice === "batu")
    ) {
      return "player win";
    }
  };

  const showResult = (result) => {
    setResultGame(result);
  };

  const reset = () => {
    setResultGame("VS");
    enableAllButtons();
  };

  const disableComp = (choice) => {
    const compButton = getCompButton(choice);
    compButton.disabled = true;
  };

  const enableAllButtons = () => {
    const buttons = document.getElementsByClassName("btn");
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].disabled = false;
    }
  };

  const getCompButton = (choice) => {
    switch (choice) {
      case "gunting":
        return document.getElementById("button-g-c");
      case "batu":
        return document.getElementById("button-b-c");
      case "kertas":
        return document.getElementById("button-k-c");
      default:
        return null;
    }
  };

  const computerChoices = ["gunting", "batu", "kertas"];

  return (
    <Layout>
      <Container fluid>
        <div className="container-game text-center justify-content-around align-content-between">
          <Row>
            <Col xs={5}>
              <h2>PLAYER 1</h2>
            </Col>
            <Col xs={2}></Col>
            <Col xs={5}>
              <h2>COMPUTER</h2>
            </Col>
          </Row>

          <Row>
            <Col xs={5}>
              <Button
                className="btn"
                id="button-b-p"
                type="button"
                onClick={() => handlePlayerPick("batu")}
              >
                <img
                  src="/img/batu.png"
                  alt="Batu"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
            <Col xs={2}></Col>
            <Col xs={5}>
              <Button className="btn" disabled id="button-b-c" type="button">
                <img
                  src="/img/batu.png"
                  alt="Batu"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
          </Row>

          <Row>
            <Col xs={5}>
              <Button
                className="btn"
                id="button-g-p"
                type="button"
                onClick={() => handlePlayerPick("gunting")}
              >
                <img
                  src="/img/gunting.png"
                  alt="Gunting"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
            <Col xs={2}>
              <h1 id="hasil">{resultGame}</h1>
            </Col>
            <Col xs={5}>
              <Button className="btn" disabled id="button-g-c" type="button">
                <img
                  src="/img/gunting.png"
                  alt="Gunting"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
          </Row>

          <Row>
            <Col xs={5}>
              <Button
                className="btn"
                id="button-k-p"
                type="button"
                onClick={() => handlePlayerPick("kertas")}
              >
                <img
                  src="/img/kertas.png"
                  alt="Kertas"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
            <Col xs={2}></Col>
            <Col xs={5}>
              <Button className="btn" disabled id="button-k-c" type="button">
                <img
                  src="/img/kertas.png"
                  alt="Kertas"
                  style={{ width: "4em", height: "4em" }}
                />
              </Button>
            </Col>
          </Row>
        </div>
      </Container>
    </Layout>
  );
};

export default RpsGame;

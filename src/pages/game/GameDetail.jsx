import axios from 'axios';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import { Button, Card, Col, Row } from "react-bootstrap";

function GameDetail(){

  const [games, setGames]= useState({});
  const params = useParams();

  const getGamesData = async () => {
    try {
      const response = await axios.get(`https://31be-production.up.railway.app/game/rooms/${params.id}`);
      setGames(response.data.data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getGamesData()
  });

  return (
    <>
    <div className="bg-gamedetail">
      <Row>
        <Col></Col>
        <Col>
          <Card style={{ width:"100 %"}} >
            <Card.Img variant="top" src={games.thumbnail_url} width={1000} />
            <Card.Body>
              <Card.Title style={{color:'black'}}>{games.name}</Card.Title>
              <Card.Text>{games.description}</Card.Text>
              <Card.Text>Player Count : {games.play_count}</Card.Text>
              <Link to={games.game_url}>
                <Button className="btn-play-now">Play Now</Button>
              </Link>
            </Card.Body>
          </Card>
        </Col>
        <Col></Col>
      </Row>
    
    </div>
    </>
  )
}

export default GameDetail
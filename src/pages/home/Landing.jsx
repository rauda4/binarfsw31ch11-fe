// Landing.jsx
import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import './landing.styles.css';

const Landing = ({ isLoggedOut }) => {
  const [username, setUsername] = useState('');

  useEffect(() => {
    const storedUsername = localStorage.getItem('username');
    setUsername(storedUsername);
  }, []);

  return (
      <div className="landingPage">
        <div className="welcomeSection">
        <Container className="container1 text-center">
        {!isLoggedOut && username && <div className='container welcome-text'>Welcome, {username}!</div>}
        <Row className="justify-content-center">
          <Col>
            <h1 className="pageTitle">This Project Binar Academy Wave 31 </h1>
            <h4 className="descriptionPage">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora ea ad
              quis, adipisci minus voluptates ullam nihil a,
              maiores odit ducimus aut nam soluta sint, hic asperiores suscipit totam
                sunt! Lorem ipsum dolor sit amet consectetur 
              adipisicing elit. Animi amet modi cum esse sit unde quisquam a. Facere, 
              soluta ducimus maiores perspiciatis fugit consectetur!
                Explicabo, quam ducimus. Ducimus, dolorum enim.
            </h4>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col className="col-6 justify-content-center">
          </Col>
        </Row>
      </Container>
        </div>
      </div>
  );
};

export default Landing;

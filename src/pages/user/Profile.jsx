import React, { useState, useEffect } from 'react';
import { Container, Button } from 'react-bootstrap';
import { PDFDownloadLink} from '@react-pdf/renderer';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './profile.css';
import ProfilePdf from '../../components/profileComponent/ProfilePdf';
import UpdateProfileModal from '../../components/profileComponent/UpdateProfileModal';
import ProfileTable from '../../components/profileComponent/ProfileTable';

const Profile = () => {
  const [email, setEmail] = useState('');
  const [totalScore, setTotalScore] = useState('');
  const [biodata, setBiodata] = useState('');
  const [city, setCity] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const username = localStorage.getItem('username');
  const userId = localStorage.getItem('userId');
  const token = localStorage.getItem('token');
  const history = useNavigate();

  useEffect(() => {
    if (!token || !userId) {
      history('/');
    } else {
      const fetchUserData = async () => {
        try {
          const response = await axios.get(`https://31be-production.up.railway.app/players/${userId}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });

          if (response.status === 200) {
            const userData = response.data.data;
            setEmail(userData.email);
            setTotalScore(userData.total_score.toString());
            setBiodata(userData.biodata);
            setCity(userData.city);
            setImageUrl(userData.image);
          }
        } catch (error) {
          console.error('Error fetching user data:', error);
        }
      };

      fetchUserData();
    }
  }, [token, userId, history]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      setIsLoading(true);
      const formData = new FormData();
      formData.append('email', email);
      formData.append('total_score', totalScore);
      formData.append('biodata', biodata);
      formData.append('city', city);
      formData.append('image', selectedImage);

      const response = await axios.put(
        `https://31be-production.up.railway.app/players/update/${userId}`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'multipart/form-data',
          },
        }
      );

      if (response.status === 200) {
        console.log('Profil berhasil diperbarui');
        const updatedUserData = response.data.data;
        setEmail(updatedUserData.email);
        setTotalScore(updatedUserData.total_score.toString());
        setBiodata(updatedUserData.biodata);
        setCity(updatedUserData.city);
        setImageUrl(updatedUserData.image);
        setSelectedImage(null);
        setShowModal(false);
      }
    } catch (error) {
      console.error('Error saat memperbarui profil:', error);
    } finally {
      setIsLoading(false);
    }
  };


  return (
    <div>
      <Container>
        <div className="card profile-card">
          <div className="profile-image">
            <h2 className="mb-4 mt-5 text-center ">{username}</h2>
            <div className="table-container mb-8">
            <ProfileTable
              imageUrl={imageUrl}
              email={email}
              totalScore={totalScore}
              biodata={biodata}
              city={city}
            />
            </div>
            <div className="button-group mb-4">
              <Button  variant="primary" onClick={() => setShowModal(true)}>
                Update profile
              </Button>
              <PDFDownloadLink className="btn ml-3" variant="secondary"
                document={
                  <ProfilePdf
                    username={username}
                    email={email}
                    totalScore={totalScore}
                    biodata={biodata}
                    city={city}
                    imageUrl={imageUrl}
                  />
                }
                fileName="profil_user.pdf"
              >
                {({ loading }) => (loading ? 'Loading...' : 'Unduh Profile')}
              </PDFDownloadLink>
            </div>
          </div>
          <UpdateProfileModal
            showModal={showModal}
            handleClose={() => setShowModal(false)}
            handleSubmit={handleSubmit}
            isLoading={isLoading}
            email={email}
            setEmail={setEmail}
            totalScore={totalScore}
            setTotalScore={setTotalScore}
            biodata={biodata}
            setBiodata={setBiodata}
            city={city}
            setCity={setCity}
            setSelectedImage={setSelectedImage}
          />
        </div>
      </Container>
    </div>
  );
};

export default Profile;

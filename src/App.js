import React, { Routes, Route, Navigate } from 'react-router-dom';
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthLayout from './layout/AuthLayout';
import GameLayout from './layout/GameLayout';
import LandingPageLayout from './layout/LandingPageLayout';
import { useEffect, useState } from 'react';

// import './App.css'

function App() {
  const token = localStorage.getItem('token');
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    if (token) {
      setIsAuthenticated(false);
    }
  }, [token]);
  return (
    <Routes>
      {/* <Route path='/*' element={<LandingPageLayout />} /> */}
      <Route path='/auth/*' element={<AuthLayout />} />
      <Route path='/game/*' element={<GameLayout />} />
      <Route
        path='/*'
        element={
          isAuthenticated ? <Navigate to={'/profile'} /> : <LandingPageLayout />
        }
      />
    </Routes>
  );
}

export default App;
